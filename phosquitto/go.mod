module github.com/ipfs/go-ipfs/examples/go-ipfs-as-a-library

go 1.14

require (
	berty.tech/go-orbit-db v1.8.0
	github.com/ipfs/go-ipfs v0.6.0
	github.com/ipfs/go-ipfs-api v0.1.0
	github.com/ipfs/go-ipfs-config v0.8.0
	github.com/ipfs/go-ipfs-files v0.0.8
	github.com/ipfs/interface-go-ipfs-core v0.3.0
	github.com/libp2p/go-libp2p-core v0.6.0
	github.com/libp2p/go-libp2p-peerstore v0.2.6
	github.com/multiformats/go-multiaddr v0.2.2
	github.com/pkg/errors v0.9.1
)
