package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	orbitdb "berty.tech/go-orbit-db"
	config "github.com/ipfs/go-ipfs-config"
	icore "github.com/ipfs/interface-go-ipfs-core"

	"berty.tech/go-orbit-db/accesscontroller"
	"berty.tech/go-orbit-db/iface"
	"github.com/ipfs/go-ipfs/core"
	"github.com/ipfs/go-ipfs/core/coreapi"
	"github.com/ipfs/go-ipfs/core/node/libp2p"
	"github.com/ipfs/go-ipfs/plugin/loader" // This package is needed so that all the preloaded plugins are loaded automatically
	"github.com/ipfs/go-ipfs/repo/fsrepo"
)

func setupPlugins(externalPluginsPath string) error {
	// Load any external plugins if available on externalPluginsPath
	plugins, err := loader.NewPluginLoader(filepath.Join(externalPluginsPath, "plugins"))
	if err != nil {
		return fmt.Errorf("error loading plugins: %s", err)
	}

	// Load preloaded and external plugins
	if err := plugins.Initialize(); err != nil {
		return fmt.Errorf("error initializing plugins: %s", err)
	}

	if err := plugins.Inject(); err != nil {
		return fmt.Errorf("error initializing plugins: %s", err)
	}

	return nil
}

func createTempRepo(ctx context.Context) (string, error) {
	repoPath, err := ioutil.TempDir("", "ipfs-shell")
	if err != nil {
		return "", fmt.Errorf("failed to get temp dir: %s", err)
	}

	// Create a config with default options and a 2048 bit key
	cfg, err := config.Init(ioutil.Discard, 2048)
	if err != nil {
		return "", err
	}

	// Create the repo with the config
	err = fsrepo.Init(repoPath, cfg)
	if err != nil {
		return "", fmt.Errorf("failed to init ephemeral node: %s", err)
	}

	return repoPath, nil
}

// Creates an IPFS node and returns its coreAPI
func createNode(ctx context.Context, repoPath string) (icore.CoreAPI, error) {
	// Open the repo
	repo, err := fsrepo.Open(repoPath)
	if err != nil {
		return nil, err
	}

	// Construct the node

	nodeOptions := &core.BuildCfg{
		Online:  true,
		Routing: libp2p.DHTOption, // This option sets the node to be a full DHT node (both fetching and storing DHT Records)
		// Routing: libp2p.DHTClientOption, // This option sets the node to be a client DHT node (only fetching records)
		Repo: repo,
		ExtraOpts: map[string]bool{
			"pubsub": true,
		},
	}

	node, err := core.NewNode(ctx, nodeOptions)
	if err != nil {
		return nil, err
	}

	// Attach the Core API to the constructed node
	return coreapi.NewCoreAPI(node)
}

// Spawns a node to be used just for this run (i.e. creates a tmp repo)
func spawnEphemeral(ctx context.Context) (icore.CoreAPI, error) {
	if err := setupPlugins(""); err != nil {
		return nil, err
	}

	// Create a Temporary Repo
	repoPath, err := createTempRepo(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to create temp repo: %s", err)
	}

	// Spawning an ephemeral IPFS node
	return createNode(ctx, repoPath)
}

func createOrbit(ctx context.Context) (iface.EventLogStore, error) {
	// Spawn a node using a temporary path, creating a temporary repo for the run
	fmt.Println("Spawning node on a temporary repo")
	ipfs, err := spawnEphemeral(ctx)
	if err != nil {
		panic(fmt.Errorf("failed to spawn ephemeral node: %s", err))
	}
	dir := "./orbitdb/benchmarks"
	defer os.RemoveAll(dir)
	fmt.Printf("creating DB\n")
	orbit, err := orbitdb.NewOrbitDB(ctx, ipfs, &orbitdb.NewOrbitDBOptions{Directory: &dir})
	if err != nil {
		fmt.Printf(err.Error())
	}
	defer orbit.Close()

	access := &accesscontroller.CreateAccessControllerOptions{
		Access: map[string][]string{
			"write": {"*", orbit.Identity().ID},
		},
	}
	overwrite := true
	fmt.Printf("creating eventlog\n")

	return orbit.Log(ctx, "DemoLog", &orbitdb.CreateDBOptions{
		AccessController: access,
		Overwrite:        &overwrite,
	})
}


func BenchmarkEventlog(b *testing.B) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	start1 := time.Now()
	fmt.Println(start1);
	db, err := createOrbit(ctx)
	if err != nil {
		fmt.Printf(err.Error())
	}
	ts := time.Since(start1)
	fmt.Printf("Spawn took %s\n", ts)

  start2 := time.Now()
	fmt.Println(start2);
	for i := 0; i < 500; i++ {// b.N; i++ { b.N=1!? probably bc also count the creation of the node
														// make subtests for the creation and additions seperatly
		db.Add(ctx, []byte("hello"))
	}
	elapsed := time.Since(start2)
	fmt.Printf("Added 500 took %s\n", elapsed)

	start3 := time.Now()
	fmt.Println(start3);
	for i := 0; i < 100; i++ {
		db.Add(ctx, []byte("hello"))
	}
	elapsed2 := time.Since(start3)
	fmt.Printf("Added 100 took %s\n", elapsed2)
}
