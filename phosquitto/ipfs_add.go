package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	shell "github.com/ipfs/go-ipfs-api"
	ipfsCore "github.com/ipfs/go-ipfs/core"
	//mock "github.com/ipfs/go-ipfs/core/mock"
	//mocknet "github.com/libp2p/go-libp2p/p2p/net/mock"
)

func main() {
	//mn := mocknet.New(ctx)
	fmt.Printf("create ipfs node\n")

	node, err := ipfsCore.NewNode(context.Context, &ipfsCore.BuildCfg{
		Online: true,
		Host:   localhost, //mock.MockHostOption(m),
		ExtraOpts: map[string]bool{
			"pubsub": true,
		},
	})

	//ipfs, err := coreapi.NewCoreAPI(node)

	//orbit, err := orbitdb.NewOrbitDB(ctx, ipfs, &orbitdb.NewOrbitDBOptions{Directory: "/mnt/home_tmp/gtec/godir/go-ipfs"})

	// Where your local node is running on localhost:5001
	sh := shell.NewShell("localhost:5001")
	cid, err := sh.Add(strings.NewReader("hello world!"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err)
		os.Exit(1)
	}
	fmt.Printf("added %s", cid)
	//sub := PubSubSubscription.new()
}
