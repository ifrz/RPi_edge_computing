package main

import (
	"fmt"
	"time"
  shell "github.com/ipfs/go-ipfs-api"
)

func main() {
	// Where your local node is running on localhost:5001
	sh := shell.NewShell("localhost:5001")

	topic := "test"
	fmt.Printf("subscribing...\n")
	sub, err := sh.PubSubSubscribe(topic)
	if err != nil {
		fmt.Printf(err.Error())
	}
	for {
	        fmt.Printf("sub: done\n") // publish messages cli $ipfs pubsub pub test $(($(date +%s%N)))
	        msg, err := sub.Next()
	        if err != nil {
		                fmt.Printf(err.Error())
	        }
	        fmt.Println(time.Now().UnixNano())
	        fmt.Println(msg.From)
	        fmt.Println(string(msg.Data))
//	fmt.Println(msg.Seqno)
	        fmt.Println(msg.TopicIDs)
//	fmt.Println(time.Now().UnixNano())
	}
	
}

